-- clear screen, UNIX
system clear;
-- or CTRL + L

-- membuat database
CREATE DATABASE [IF NOT EXISTS] `nama_database`;

-- menampilkan database 
SHOW DATABASES;

-- membuka database 
USE `nama_database`;

-- menghapus database 
DROP DATABASE [IF NOT EXISTS] `nama_database`;

-- menampilkan tabel
SHOW TABLES;

-- membuat tabel baru 
CREATE TABLE `name_tabel` (
    `field1` `tipe(panjang)`, 
    `field2` `tipe(panjang)` NOT NULL DEFAULT , 
    `PRIMARY KEY (field_key)`
);

-- melihat struktur tabel 
DESC `nama_tabel`;

-- mengubah struktur tabel
-- menambah field baru 
ALTER TABLE `nama_tabel` ADD `field` `tipe(panjang)` NOT NULL AFTER `nama_field`;
-- hapus primary key
ALTER TABLE `nama_table` DROP PRIMARY KEY;
-- menambah primary key
ALTER TABLE `nama_table` ADD PRIMARY KEY (`field`);
-- mengubah tipe field
ALTER TABLE `nama_table` MODIFY `field` `tipe(panjang)` NOT NULL;