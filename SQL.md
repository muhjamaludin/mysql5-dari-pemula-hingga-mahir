Dasar - dasar SQL
- Jenis Perintah SQL
	- DDL (Data definition language)
		- CREATE
		- ALTER
		- RENAME
		- DROP
	- DML (Data manipulation language)
		- SELECT
		- INSERT
		- UPDATE
		- DELETE
	- DCL (Data control language)
		- GRANT
		- REVOKE

Alter options
- ADD `field_baru`
- ADD INDEX `nama_index`
- ADD PRIMARY KEY (`field_kunci`)
- CHANGE `field_yang_diubah` `definisi_field_baru`
- MODIFY `definisi_field`
- DROP `nama_field`
- RENAME TO `nama_tabel_baru`