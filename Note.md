Software DBMS
- DB2
- Microsoft SQL Server
- Oracle
- Sybase
- Interbase
- Teradata
- Firebird
- MySQL
- PostgreSQL

MySQL
- multithread
- multi-user
- fitur
	- Relational Database System
	- Arsitektur Client-Server
	- Mengenal perintah SQL standar
	- mendukung Sub Select
	- mendukung Views
	- mendukung Stored Prosedured(SP) 
	- mendukung Triggers
	- mendukung replication
	- mendukung transaksi
	- mendukung foreign key
	- tersedia fungsi GIS
	- free
	- stabil dan tangguh
	- fleksibel dengan berbagai pemrograman
	- security baik
	- dukunga komunitas
	- perkembangan software cukup cepat

MySQL Client untuk Administrasi Server MysQL
- MySQL Command Line Client
- MySQL-Front
- PHPMyAdmin
- SQLYog
- MySQL Administrator
- MySQL Query Browser
- MySQL Workbench

Tipe -tipe Tabel MySQL
- MyISAM
	- kelebihan
		- kecepatan
		- kestabilan
	- fungsi
		- tabel sederhan yang tidak terlalu rumit
	- jenis tabel
		- MyISAM static
		- MyISAM dynamic
		- MyISAM compressed
- InnoDB
	- keunggulan
		- mendukung transaksi antar tabel
		- mendukung row-level-locking
		- mendukung foreign-key constraints
		- crash recovery
- HEAP
	- menyimpan data di RAM
- Tipe tabel lain
	- BDB
	- Archieve
	- CSV (data dalam bentuk file text yang dibatasi koma sbg delimiter)
	- NDB Table (MySQL Cluster)
	- Federated (External Tables)

Tipe - tipe Filed (Kolom) MYSQL
- Tipe Numeric
	- TINYINT (1 byte) :: -128 s/d 127
	- SMALLINT (2 byte) :: -32.768 s/d 767
	- MEDIUMINT (3 byte) :: -8.388.608 s/d 8.388.607
	- INT (4 byte) :: -2.147.483.648 s/d 2.147.483.647
	- BIGINT (8 byte) :: +- 9,22 x 10<sup>18</sup>
	- FLOAT (4 byte) :: data pecahan presisi tunggal
		- good for divisions
		- store in binary like m*2^n
	- DOUBLE (8 byte) :: data pecahan presisi ganda
	- REAL -- sinonim DOUBLE
	- DECIMAL (8 byte) :: data pecahan
		- good for sum
		- store in number like m*10^n 
	- NUMERIC -- sinonim DECIMAL
- Tipe Data dan Time
	- DATE (3 byte) :: YYYY-MM-DD
	- TIME (3 byte) :: HH:MM:SS
	- DATETIME (8 byte) :: YYYY-MM-DD HH:MM:SS
	- YEAR (1 byte) :: YYYY
- Tipe String (Text)
	- CHAR (0 s/d 255 karakter) :: fixed length
	- VARCHAR (0 s/d 65.535) :: dynamic length
		- dapat di index
		- dapat di limit
	- TINYTEXT (0 s/d 255)
	- TEXT (2 <sup>16</sup> - 1)
		- tidak dapat di index
		- tidak dapat di limit
	- MEDIUMTEXT (2<sup>24</sup> - 1)
	- LONGTEXT (2<sup>32</sup> - 1)
- Tipe BLOB (Binary Large Object) :: menyimpan kode biner dr file/object.
	- BIT (64 digit biner)
	- TINYBLOB (255 byte)
	- BLOB (2<sup>16</sup> - 1)
	- MEDIUMBLOB (2<sup>24</sup> - 1 byte)
	- LONGBLOB (2<sup>32</sup> - 1 byte)
- Tipe data lain
	- ENUM :: kumpulan data
	- SET :: himpunan data


Aturan Merancang Database yang baik
1. Tabel dalam database tidak boleh mengandung redundancy / record (data) ganda.
2. Setiap tabel dalam database, harus memiliki field (kolom) yang unik. Primay Key.
3. Tabel harus sudah normal.
4. Besar atau ukuran database hendaknya dibuat seminimal mungkin. Ditentukan oleh pemilihan tipe data.
5. Daya tampung data (record) sesuai dengan kebutuhan aplikasi.

Tipe penamaan identifier
a. case sensitive. consistent (lower-case, upper-case, camelCase, dll)
b. nama database, tabel, dan kolom maksimal 64 karakter.
c. hindari penggunaan karakter khusus
d. pilih nama untuk filed (kolom) yang mencerminkan isi dari data yang disimpan.
